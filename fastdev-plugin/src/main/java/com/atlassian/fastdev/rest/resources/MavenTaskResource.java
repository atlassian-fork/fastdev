package com.atlassian.fastdev.rest.resources;

import java.util.UUID;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.fastdev.maven.MavenTask;
import com.atlassian.fastdev.maven.MavenTaskManager;
import com.atlassian.fastdev.rest.FastdevUriBuilder;
import com.atlassian.fastdev.rest.representations.MavenTaskCollectionRepresentation;
import com.atlassian.fastdev.rest.representations.MavenTaskRepresentation;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/tasks")
@AnonymousAllowed
public class MavenTaskResource
{
    private final MavenTaskManager taskManager;
    private final FastdevUriBuilder uriBuilder;

    public MavenTaskResource(MavenTaskManager taskManager, FastdevUriBuilder uriBuilder)
    {
        this.taskManager = checkNotNull(taskManager, "taskManager");
        this.uriBuilder = checkNotNull(uriBuilder, "uriBuilder");
    }

    @GET
    @Produces(APPLICATION_JSON)
    public Response getTaskCollection()
    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String json = gson.toJson(new MavenTaskCollectionRepresentation(taskManager, uriBuilder));

        return Response.ok(json, MediaType.APPLICATION_JSON).build();
        
    }

    @GET
    @Path("{uuid}")
    public Response getTask(@PathParam("uuid") String uuid)
    {
        MavenTask task = taskManager.getTask(UUID.fromString(uuid));
        if (task == null)
        {
            task = taskManager.getCompletedTask(UUID.fromString(uuid));

            if (task == null)
            {
                return Response.status(404).build();
            }
        }

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String json = gson.toJson(new MavenTaskRepresentation(task, uriBuilder));

        return Response.ok(json, MediaType.APPLICATION_JSON).build();
        
    }
}

