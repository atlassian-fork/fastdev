package com.atlassian.livereload.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.templaterenderer.TemplateRenderer;

/**
 * This is a simple servlet for testing live reload.
 * 
 * @since 1.10
 */
public class LiveReloadTestServlet extends HttpServlet
{
    public static final String RESULT_TEMPLATE = "/template/livereload-test.vm";
    private final TemplateRenderer renderer;

    public LiveReloadTestServlet(TemplateRenderer renderer)
    {
        this.renderer = renderer;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        final Map<String, Object> context = new HashMap<String, Object>();

        response.setContentType("text/html;charset=utf-8");
        renderer.render(RESULT_TEMPLATE, context, response.getWriter());
    }
}
