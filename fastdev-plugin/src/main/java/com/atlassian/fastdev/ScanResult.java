package com.atlassian.fastdev;

import java.io.File;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @since version
 */
public class ScanResult
{
    private File buildRoot;
    private File root;
    private boolean pomChanged;
    private File rootPom;

    public ScanResult(File buildRoot, File root, boolean pomChanged, File rootPom)
    {
        this.buildRoot = buildRoot;
        this.root = root;
        this.pomChanged = pomChanged;
        this.rootPom = rootPom;
    }

    public File getBuildRoot()
    {
        return buildRoot;
    }

    public File getRoot()
    {
        return root;
    }

    public boolean isPomChanged()
    {
        return pomChanged;
    }

    public File getRootPom()
    {
        return rootPom;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass())
        {
            return false;
        }
        ScanResult other = (ScanResult) obj;
        return new EqualsBuilder()
                .append(buildRoot, other.buildRoot)
                .append(root, other.root)
                .append(pomChanged, other.pomChanged)
                .append(rootPom, other.rootPom)
                .isEquals();

    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(buildRoot).append(root).append(pomChanged).append(rootPom).toHashCode();
    }

    @Override
    public String toString()
    {
        return new ToStringBuilder(this)
                .append("buildRoot", buildRoot)
                .append("root", root)
                .append("pomChanged", pomChanged)
                .append("rootPom",rootPom)
                .toString();
    }
}
