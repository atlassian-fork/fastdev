package com.atlassian.fastdev.util;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;


/**
 * @since version
 */
public class PomCLIChecker
{
    public static Boolean useFastdevCli(File pomFile)
    {
        Boolean useCli = true;
        
        try
        {
            String pomXml = FileUtils.readFileToString(pomFile);
            String flag = StringUtils.substringBetween(pomXml, "<useFastdevCli>", "</useFastdevCli>");

            if(StringUtils.isNotBlank(flag))
            {
                if("false".equalsIgnoreCase(StringUtils.trim(flag)))
                {
                    useCli = false;
                }
            }

        }
        catch (Exception e)
        {
           //ignore
        }
        
        return useCli;
    }
}
