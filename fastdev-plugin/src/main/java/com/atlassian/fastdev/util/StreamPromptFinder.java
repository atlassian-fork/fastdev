package com.atlassian.fastdev.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;

/**
 * @since version
 */
public class StreamPromptFinder extends Thread
{
    private final InputStream is;
    private final ConcurrentLinkedQueue<String> output;
    private final Logger logger;
    private boolean killIt;

    public StreamPromptFinder(InputStream is, ConcurrentLinkedQueue<String> output, Logger logger)
    {
        this.is = is;
        this.output = output;
        this.logger = logger;
        this.killIt = false;
        start();
    }

    public void run()
    {
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new InputStreamReader(is));
            int c;
            StringBuffer lineBuffer = new StringBuffer();
            
            int promptsFound = 0;
            boolean newBuffer = true;
            while ((c = br.read()) > -1 && !killIt)
            {
                lineBuffer.append((char) c);
                logger.debug("eat c: " + (char) c);
                
                if(lineBuffer.toString().startsWith("maven>") && newBuffer)
                {
                    promptsFound++;
                    logger.debug("got " + promptsFound + " prompts");
                    newBuffer = false;
                }
                
                if ((c == '\n') || (c == '\r'))
                {
                    String line = lineBuffer.toString();
                    logger.debug("eating: " + line);
                    output.add(line);
                    lineBuffer = new StringBuffer();
                    newBuffer = true;
                }
                
                if(promptsFound > 1)
                {
                    logger.debug("exiting");
                    killIt = true;
                    break;
                }
            }
        }
        catch (IOException ioe)
        {
            //eat some more.
        }
        finally {
            
            if(null != br)
            {
                IOUtils.closeQuietly(br);
            }

            if(null != br)
            {
                IOUtils.closeQuietly(br);
            }
        }
    }

    public void kill()
    {
        this.killIt = true;
    }
}
