AJS.$(function ($) {

    var liveReloadUrl = function () {
        return AJS.contextPath() + '/download/resources/com.atlassian.labs.fastdev-plugin/js/livereload.js?host='+location.hostname+'&port=35730';
    };

    function setupBinds(){
        // bind to the live reload toolbar icon
        $('body').delegate('#dt-live-reload', 'click', function () {

            if ($(this).hasClass('dt-reload-on')) {
                disableLiveReload();
            }
            else {
                enableLiveReload();
            }
        });

        // bind to the scan and reload toolbar icon
        $('body').delegate('#dt-scan-and-reload', 'click', function () {
            if($('.dt-msg-fastdev.dt-msg-show').length > 0) {
                return false;
            }
            var self = $(this).addClass('dt-reload-on');
            console.log('Scanning for changes to compile-able sources...');
            $.ajax({
                type: 'POST',
                url: AJS.contextPath() + '/rest/fastdev/1.0/reload',
                dataType: 'json',
                contentType: 'application/json',
                success: function(d,s,x){
                    self.removeClass('dt-reload-on');
                    if (x.status === 204) {
                        console.log('... no changes detected.');
                        $('.dt-msg-fastdev').addClass('dt-msg-show').animate({
                            bottom: '+=85',
                            opacity: 1
                        },'2000','swing',function(){
                            var self = $(this);
                            setTimeout(function(){
                                self.animate({
                                    bottom: '-=85',
                                    opacity: 0
                                });
                                self.removeClass('dt-msg-show');
                            }, 1000);
                        });
                    } else {
                        window.location = AJS.contextPath() + '/plugins/servlet/fastdev?origUrl=' + encodeURIComponent(window.location.href);
                    }
                }
            });
        });

    }

    var fireEvent = function (element, event) {
        if (document.createEventObject) {
            // dispatch for IE
            var evt = document.createEventObject();
            evt.type = event;
            element.fireEvent(event, evt)
        }
        else {
            // dispatch for firefox + others
            var evt = document.createEvent("HTMLEvents");
            evt.initEvent(event, true, true); // event type,bubbling,cancelable
            element.dispatchEvent(evt);
        }
    };

    var turnOnSpinner = function(){
        if($('#dt-live-reload').length == 0) {
            setTimeout(turnOnSpinner, 100);
        } else {
            $('#dt-live-reload').addClass('dt-reload-on');
            console.log('Live Reload is on');
        }
    };

    var enableLiveReload = function () {
        if (localStorage) {
            localStorage.setItem("atlassianLiveReload", window.location.pathname);
        }

        var lrscript = document.createElement('script');
        lrscript.type = 'text/javascript';
        lrscript.id = 'lrjstag';
        document.body.appendChild(lrscript);
        lrscript.src = liveReloadUrl();
        turnOnSpinner();
    };

    var disableLiveReload = function () {
        $('#dt-live-reload').removeClass('dt-reload-on');
        if (localStorage) {
            localStorage.removeItem("atlassianLiveReload");
        }
        fireEvent(document, 'LiveReloadShutDown');
        $('#lrjstag').remove();
        console.log('Live Reload is off');
    };

    var init = function (retryCount) {
        // wait for dtToolbar to be initialized
        // this is in case the developer toolbox is not installed
        // or the version of the toolbox doesn't support the toolbar -- highly unlikely
        if (typeof dtToolbar === "undefined") {
            retryCount += 1;
            if (retryCount < 5) {
                setTimeout(function(){
                    init(retryCount)
                }, 1000);
            }
            return false;
        }
        if (localStorage) {
            var lrPath = localStorage.getItem("atlassianLiveReload");
            if (lrPath === window.location.pathname) {
                enableLiveReload();
            }
        }
        // Commented out for now since the whenIType method fails hard if there's a
        // conflicting key
//        AJS.whenIType('lr').execute(function(){
//            if ($('#dt-live-reload').hasClass('dt-reload-on')) {
//                disableLiveReload();
//            } else {
//                enableLiveReload();
//            }
//        });

        $('<div class="dt-msg dt-msg-fastdev">No changes found</div>').appendTo('body');

        setupBinds();
    };

    init(0);

});

        
