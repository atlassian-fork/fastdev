package com.atlassian.livereload;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;

import org.apache.commons.io.DirectoryWalker;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import name.pachler.nio.file.*;

/**
 * This class uses the WatchService API to listen for filesystem change events and broadcast them for live reloading.
 * Currently we use the jpathwatch library which is a backport of the Java 7 APIs for Java 5/6.
 * 
 * When a filesystem change event is detected, we make sure the file is in our list of approved reload extensions.
 * We also gather all of the events and only broadcast a single event per file.
 * (e.g. linux gives both a delete and create for mods but we only want 1 change event)
 * 
 * @since 1.10
 */
public class LiveReloadFileWatcher implements InitializingBean, DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(LiveReloadFileWatcher.class);

    //the list of extensions we currently handle
    private static final Set<String> EXTENSIONS = new HashSet<String>()
    {{
        add("css");
        add("js");
            add("png");
            add("gif");
            add("jpg");
            add("vm");
            add("html");
            add("htm");
            add("ftl");
            add("soy");
            add("less");
    }};
    
    //the watch service needs to be run in it's own thread
    private final ExecutorService exec = Executors.newSingleThreadExecutor();

    private final WatchService watchService;
    private final EventPublisher eventPublisher;
    private final Map<WatchKey,Path> keyedPaths;

    public LiveReloadFileWatcher(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
        this.watchService = FileSystems.getDefault().newWatchService();
        this.keyedPaths = new HashMap<WatchKey, Path>();
    }

    public void afterPropertiesSet() throws Exception
    {
        addRoots(System.getProperty(AlternativeDirectoryResourceLoader.PLUGIN_RESOURCE_DIRECTORIES));

        Runnable task = new Runnable()
        {
            public void run()
            {
                startWatching();
            }
        };

        exec.execute(task);
    }

    private void startWatching()
    {
        log.debug("starting file watcher...");
        while (!exec.isShutdown())
        {
            // take() will block until a file has been created/deleted
            WatchKey signalledKey;
            try
            {
                signalledKey = watchService.take();
            }
            catch (InterruptedException ix)
            {
                // we'll ignore being interrupted
                continue;
            }
            catch (ClosedWatchServiceException cwse)
            {
                // other thread closed watch service
                log.debug("watch service closed, terminating.");
                break;
            }

            // get list of events from key
            List<WatchEvent<?>> list = signalledKey.pollEvents();

            // VERY IMPORTANT! call reset() AFTER pollEvents() to allow the
            // key to be reported again by the watch service
            signalledKey.reset();

            //get the root dir
            Path parentPath = keyedPaths.get(signalledKey);
            
            if(null != parentPath)
            {
                Set<String> updatePaths = new HashSet<String>();
                
                //loop over the events and grab the ones we care about
                for (WatchEvent e : list)
                {
                    String message = "";
                    
                    if (e.kind() == StandardWatchEventKind.ENTRY_CREATE 
                            || e.kind() == StandardWatchEventKind.ENTRY_DELETE 
                            || e.kind() == StandardWatchEventKind.ENTRY_MODIFY)
                    {
                        //need to create the full path to the file
                        Path context = (Path) e.context();
                        String fullPath = parentPath.toString() + "/" + context.toString();
                        
                        //if it's a file we care about, add it to the set
                        if(EXTENSIONS.contains(FilenameUtils.getExtension(fullPath)))
                        {
                            updatePaths.add(fullPath);
                        
                            message = context.toString() + " " + e.kind().name();
                        }
                    }
                    //if we get too many events, we just won't fire a change
                    else if (e.kind() == StandardWatchEventKind.OVERFLOW)
                    {
                        message = "OVERFLOW: more changes happened than we could retrieve";
                    }

                    if (StringUtils.isNotBlank(message))
                    { 
                        log.debug(message);
                    }
                }
                
                //publish the change events for live reload broadcast
                for(String updatedPath : updatePaths)
                {
                    eventPublisher.publish(new LiveReloadEvent(updatedPath));
                }
            }
        }
    }

    public void destroy() throws Exception
    {
        exec.shutdown();
        watchService.close();
    }

    private void addRoots(String dirs)
    {
        if (dirs != null)
        {
            for (String dir : dirs.split(","))
            {
                File resourceDir = new File(dir.trim());
                if (!resourceDir.exists())
                {
                    log.warn("Plugin resource directory '" + resourceDir.getPath().toString() + "' does not exist. Ignoring");
                    continue;
                }
                else
                {
                    try
                    {
                        //we need to recurse because we have to add every dir to the watch service individually
                        ResourceDirectoryWalker walker = new ResourceDirectoryWalker();
                        List<File> allDirs = walker.getResourceDirs(resourceDir);
                        for (File watchDir : allDirs)
                        {
                            log.info("Found plugin reload target directory '{}'", watchDir);
                            Path watchPath = Paths.get(watchDir.getCanonicalPath());
                            WatchKey key = watchPath.register(watchService, StandardWatchEventKind.ENTRY_CREATE, StandardWatchEventKind.ENTRY_DELETE, StandardWatchEventKind.ENTRY_MODIFY);
                            
                            //add a key/path entry so we can get the parent dir later on
                            keyedPaths.put(key,watchPath);
                        }
                    }
                    catch (IOException e)
                    {
                        //we don;t really care
                        e.printStackTrace();
                    }

                }
            }
        }
    }

    private class ResourceDirectoryWalker extends DirectoryWalker
    {
        public List<File> getResourceDirs(File startDir) throws IOException
        {
            List<File> results = new ArrayList<File>();
            walk(startDir, results);

            return results;
        }

        protected boolean handleDirectory(File directory, int depth, Collection results) throws IOException
        {
            if (directory.isDirectory() && directory.exists() && !results.contains(directory))
            {
                results.add(directory);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
