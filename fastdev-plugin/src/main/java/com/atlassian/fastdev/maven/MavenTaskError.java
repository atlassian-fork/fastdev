package com.atlassian.fastdev.maven;

public enum MavenTaskError
{
    PLUGIN_INSTALL_IN_PROGRESS,
    INVALID_BUILD_ROOT,
    UNABLE_TO_START_CLI
}