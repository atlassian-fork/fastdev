package com.atlassian.fastdev.pageobjects;

import java.util.ArrayList;
import java.util.Collection;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;
import com.google.inject.Inject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FastdevServletPage implements Page
{
    private static final String PAGE_URL = "/plugins/servlet/fastdev";
    private static final String TRIGGER_CLASS = "trigger-reload";

    @Inject private AtlassianWebDriver driver;
    @Inject private PageBinder binder;

    @FindBy(id="content")
    private WebElement content;

    @WaitUntil
    public void waitUntilPageIsLoaded()
    {
        driver.waitUntil(loaded(content));
    }

    public String getUrl()
    {
        return PAGE_URL;
    }

    public boolean isScanButtonEnabled()
    {
        return getScanButton().isEnabled();
    }

    public void scanAndReload()
    {
        getScanButton().click();
    }

    public Collection<Task> getTasks()
    {
        Collection<Task> tasks = new ArrayList<Task>();
        for (WebElement element : getActiveContainer().findElements(By.className("task")))
        {
            tasks.add(new Task(element));
        }
        return tasks;
    }
    
    public void waitUntilTaskStarts()
    {
        driver.waitUntilElementIsVisible(By.className("progress-icon"));
    }

    private WebElement getScanButton()
    {
        return getActiveContainer().findElement(By.className(TRIGGER_CLASS));
    }

    private WebElement getActiveContainer()
    {
        for (WebElement element : content.findElements(By.tagName("div")))
        {
            if (element.isDisplayed())
            {
                return element;
            }
        }
        throw new RuntimeException("No visible DIV found");
    }

    private static Function<WebDriver, Boolean> loaded(final WebElement element)
    {
        return new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return !element.getAttribute("class").contains("loading");
            }
        };
    }
}
