Fastdev = Fastdev || {};

Fastdev.template = (function() {
    var templates = {};

    function loadTemplate(name) {
        var template = templates[name] = templates[name] || $($('#' + name + '-template').html());
        return template.clone();
    }

    function fillTemplate(name, data) {
        var template = loadTemplate(name);
        for (var i = 0, len = data.length; i < len; i++) {
            var item = data[i];
            if (item) {
                var el = template.find('.' + item.name);

                if (item.html) {
                    el.html(item.html);
                } else if (item.text) {
                    el.text(item.text);
                }

                if (item.attr) {
                    for (attr in item.attr) {
                        if (attr === 'className') {
                            el.addClass(item[attr]);
                        } else {
                            el.attr(attr, item.attr[attr]);
                        }
                    }
                }
            }
        }
        return template;
    }

    return {
        load: loadTemplate,
        fill: fillTemplate
    }

})();